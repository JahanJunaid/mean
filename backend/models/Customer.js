const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define collection and schema
let Customer = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    userName: {
      type: String,
    },
    email: {
      type: String,
    },
    dob: {
      type: Date,
    },
    phone: {
      type: Number,
    },
    gender:{
      type: String
    },
    password:{
      type: String
    },
    image:{
      type: String
    }
  },
  {
    collection: "costomers",
  }
);

module.exports = mongoose.model("Customer", Customer);
