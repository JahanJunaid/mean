import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { SearchComponent } from './components/search/search.component';
import { AddFormComponent } from './components/add-form/add-form.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { CustomerNameHeaderComponent } from './components/customer-details/customer-name-header/customer-name-header.component';
import { CustomerAddressComponent } from './components/customer-address/customer-address.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    CustomerListComponent,
    SearchComponent,
    AddFormComponent,
    CustomerDetailsComponent,
    CustomerNameHeaderComponent,
    CustomerAddressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
