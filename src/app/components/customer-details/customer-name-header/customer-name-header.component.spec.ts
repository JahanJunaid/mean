import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerNameHeaderComponent } from './customer-name-header.component';

describe('CustomerNameHeaderComponent', () => {
  let component: CustomerNameHeaderComponent;
  let fixture: ComponentFixture<CustomerNameHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerNameHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerNameHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
