export class Employee {
  firstName: string;
  email: string;
  lastName: string;
  userName: string;
  dob: string;
  phone: string;
  gender: string;
  password: string;
  image: string;
}
